class Fibonacci
    def valueforterm(term)
        @term = term
        
        if @term < 1
            return 0
        end
        
        if @term == 1 || @term == 2
            return 1
        end
        
        @prevterm_val = 1
        @currterm_val = 1
        @nextterm_val = 2
        @currterm = 2
        
        while @currterm < @term
            @nextterm_val = @prevterm_val + @currterm_val
            @prevterm_val = @currterm_val
            @currterm_val = @nextterm_val
            @currterm += 1
        end
        
        return @currterm_val
    end
end