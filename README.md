# BDD project using cucumber (http://cukes.info/)

Feature: Fibonacci

	As a student
	I want to find the correct nth term in the Fibonacci sequence
	So that I can complete my assignment with confidence