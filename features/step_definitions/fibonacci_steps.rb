# encoding: utf-8
begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end 
require 'cucumber/formatter/unicode'
$:.unshift(File.dirname(__FILE__) + '/../../lib')
require 'fibonacci'

Before do
  @fibonacci = Fibonacci.new
end

After do
end

When /I ask for term number (.*)/ do |term|
  @result = @fibonacci.valueforterm(term.to_i)
end

Then /the result should be (.*)/ do |result|
  @result.should == result.to_i
end