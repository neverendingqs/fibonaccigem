Feature: Fibonacci
	As a student
	I want to find the correct nth term in the Fibonacci sequence
	So that I can complete my assignment with confidence
	
	Scenario Outline: Find the nth term in the Fibonacci sequence
	
		When I ask for term number <term>
		Then the result should be <result>
		
		Examples:
	    | term | result                                                          |
	    | 1    | 1                                                               |
	    | 2    | 1                                                               |
	    | 3    | 2                                                               |
	    | 4    | 3                                                               |
	    | 5    | 5                                                               |
	    | 6    | 8                                                               |
	    | 7    | 13                                                              |
	    | 300  | 222232244629420445529739893461909967206666939096499764990979600 |